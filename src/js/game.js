"use strict";

function Game(){
	return {
		canvas: {},
		ctx: {},
		fpsLimit: 10,
		drawPool: [],
		cellSize: 32, //Cell size in pixels
		mapObjects:[], //Objects in matrix x,y,z
		mapSize:{x: 10, y: 5, z: 10}, // Map size
		mapOffset: {x: 0, y: 0}, //Offset of drawn objects (For drawing sprites in center of screen)
		
		Init: function(canvas_id, mapSize){ //Initialization (Canvas ID, MapSize: {x:0 , y:0, z:0})
			this.mapSize = mapSize;
			console.log(canvas_id);
			this.canvas = document.getElementById(canvas_id);
			this.ctx = this.canvas.getContext("2d");
            this.canvas.width = window.innerWidth;
            this.canvas.height = window.innerHeight;
			this.mapOffset.x = this.canvas.width / 2;
			this.mapOffset.y = this.canvas.height / 2;
			this.mapOffset.x -= this.mapSize.x * this.cellSize / 2;
			this.mapOffset.y += this.mapSize.y * this.cellSize / 4;
			
		},
		
		Draw: function(){ //Just clearing last frame and draw background color
			this.ctx.fillStyle = "#91b7ce";
			this.ctx.fillRect(0,0,2000,2000);
		},
		
		DrawImage: function(src, x, y){ //Sprite drawing
			this.ctx.drawImage(src, x, y, 32, 32);
		},
		
		DrawMap: function(){ //Sraw all objects inside mapObjects matrix
			for(var y = 0; y < this.mapSize.y; y++){ // First of all we draw lower cells (lowest y value) "the ground"
				for(var z = 0; z < this.mapSize.z; z++){ // Now the Z value, lowest is 0 (more further from camera)
					for(var x = this.mapSize.x - 1; x >= 0; x--){ // And now X. X sorted DESC because right object has to be overrided by object from left side
						if(this.mapObjects[x][y][z] && this.mapObjects[x][y][z].visible == true){//If cell is filled with Block and this block is (visible = true)
							
							this.DrawImage(
								this.mapObjects[x][y][z].img,
								x * (this.cellSize / 2 ) + z * (this.cellSize / 2) + this.mapOffset.x,
								x * (-this.cellSize / 4) + z * (this.cellSize / 4) + this.mapOffset.y - y * (this.cellSize / 2 )
							);
							
						}
					}
				}
			}
			
		},
		
		GenerateMap: function(){ // Map generator ( Filling cells with blocks )
			var imgBlockGreen=document.getElementById('imgBlock'); 		//Sprite with Green block
			var imgBlockShort=document.getElementById('imgBlockShort'); //Sprite with Half Green block
			var imgBlockBlack=document.getElementById('imgBlockBlack'); //Sprite with Gray block
			this.mapObjects = [];
			
			for(var x = 0; x < this.mapSize.x; x++){
				this.mapObjects[x] = [];
				
				for(var y = 0; y < this.mapSize.y; y++){
					this.mapObjects[x][y] = [];
					
					for(var z = 0; z < this.mapSize.z; z++){
						if(Math.random() < 0.9){ //Put block into cell with 90% chance
							this.mapObjects[x][y][z] = new Block();
							this.mapObjects[x][y][z].setPosition( x, y, z, this.cellSize );
							this.mapObjects[x][y][z].img = Math.random() > 0.5 ? imgBlockBlack : imgBlockGreen;// Set block image black or green (50/50)
							this.mapObjects[x][y][z].visible = false;
						}
						
					}
				}
			}
		}
	};
}