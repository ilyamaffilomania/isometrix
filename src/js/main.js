"use strict";
var game = new Game(); //Game object
var pong = 1; //Pong animation (show layers and hide them)
var layer = -1; //Current layer for animation (starting from -1 because with first loop it will be increased by pong value (1) )

game.Init("gameCanvas" , {x: 20, y: 25, z: 20} );//Game initialization + size of game map (game world)
game.GenerateMap(); // Generating map

setInterval(function(){//Animation of layers(y) show/hide
	game.Draw(); //Clearing screen and colorizing background with blue sky color
	game.DrawMap(); // Draw all cells that are visible (Block.visible == true)
	
	if( (pong > 0 && layer >= game.mapSize.y) || (pong < 0 && layer <= 0) ){ //Change pong to 1/-1 if it greater mapSize.y or less than 0
		pong *= -1;
	}
	layer += pong;
	
	var y = layer;
	for(var x = 0; x < game.mapSize.x; x++){
		for(var z = 0; z < game.mapSize.z; z++){
			if(game.mapObjects[x][y][z]){
				if(pong > 0)
					game.mapObjects[x][y][z].visible = true; //Set block visible if we increasing layer value
				else
					game.mapObjects[x][y][z].visible = false; //Set block hidden if we decreasing layer value
			}
		}
	}
},1000 / game.fpsLimit);